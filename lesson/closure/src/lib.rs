fn call_with_one<F>(f: F) -> i32
    where F: Fn(i32) -> i32, {
    f(1)
}

// Higher-Rank Trait Bounds (HRTBs)
// for<'a> can be read as "for all choices of 'a"
fn call_with_ref<F>(x: &i32, f:F) -> i32
    where F: for<'a> Fn(&'a i32) -> i32 {
    f(&x)
}

fn echo(x: &i32) -> &i32 {
    x
}


extern crate time;
use time::PreciseTime;

#[cfg(test)]
mod tests {
        
    use super::*;
    
    #[test]
    fn it_works() {
    }
    
    #[test]
    fn test_call_with_one() {
        assert_eq!(1, call_with_one(|x|{x}));
        assert_eq!(2, call_with_one(|x|{ x+1 }));
    }
   
    #[test] 
    fn test_call_with_ref() {
        let x: i32 = 5;
        assert_eq!(5, call_with_ref(&x, |&y| y));
        assert_eq!(5, x); 
    }
 
    #[test]
    fn test_echo() {
        let y: i32 = 13;
        let &z = echo(&y);
        assert_eq!(13, z);
    }   
    #[test]
    fn test_loop() {
        let mut x: Vec<f64> = Vec::new();
        let mut y: Vec<f64> = Vec::new();
        let n: usize = 1000000;
        x.resize(n, 1.);
        y.resize(n, 0.);
        
        let a:f64 = 5.;
        let b:f64 = 7.;
        
        let start = PreciseTime::now();
        for j in 0..100 { 
        for i in 0..n {
          y[i] = a*x[i] + b;    
        }
        }
        let end = PreciseTime::now();        
        
         
    

        println!("{} seconds for adding {} integers.", start.to(end), n);
    } 
        
}
