use std::f64;

///
/// Panic with a message unless the two floating numbers are equal.
///
/// # Example
///
/// ```
/// // Error
/// assert_eqf64!(1. as f64, 2. as f64);
/// // OK
/// assert_eqf64!(1. as f64, 1. as f64);
/// ```
///
#[macro_export]
macro_rules! assert_eqf64 {
    ($a: expr,$b: expr) => ({
        match (&($a), &($b)) {
            (l,r) => {
                let abs_l = l.abs();
                let abs_r = r.abs();
                let diff = (*l - *r).abs();

                let ans: bool;

                if *l == *r {
                    ans = true;
                } else if *l == 0.0 || *r == 0.0 || diff < f64::MIN_POSITIVE {
                    // One of a or b is zero (or both are extremely close to it,) use absolute error.
                    ans = diff < (f64::EPSILON * f64::MIN_POSITIVE);
                } else {
                    // Use relative error.
                    ans = (diff / f64::min(abs_l + abs_r, f64::MAX)) < f64::EPSILON;
                }

                if !ans {
                    panic!("assertion failed: `(left == right)` (left: `{}`, right: `{}`)", *l, *r);
                }
            }
        }
    })
}

///
/// A collection of methods for area related calculations
///
pub trait HasArea {
    /// Returns the area
    fn area(&self) -> f64;
    /// Return the area comparison of `self > rhs`  
    /// 
    /// * `rhs` - right hand side variable
    fn is_larger<T: HasArea>(&self, rhs: &T) -> bool {
        self.area() > rhs.area()
    }
}

///
/// Circle
///
#[derive(PartialEq, Debug)]
pub struct Circle {
    x: f64,
    y: f64,
    r: f64,
}

///
/// Square
///
pub struct Square {
    x: f64,
    y: f64,
    side: f64,
}

impl Circle {
    fn area(&self) -> f64 {
        f64::consts::PI * self.r.powi(2)
    }
}

impl HasArea for Circle {
    fn area(&self) -> f64 {
        self.area()
    }
    
//    fn is_larger(&self, rhs: &Self) -> bool {
//        self.area() > rhs.area()
//    }
}

impl HasArea for Square {
    fn area(&self) -> f64 {
        self.side * self.side
    }   
}


pub fn print_area<T: HasArea>(shape: T) {
    println!("This shape has an area of {}", shape.area());
}
/// Rectangle
pub struct Rectangle<T> {
    x: T, y: T, height: T, width: T,
}

impl <T: PartialEq> Rectangle<T> {
    /// Check if a Rectangle is a square
    fn is_square(&self) -> bool {
       self.width == self.height
    }
}

#[cfg(test)]
mod tests {
    use super::*;
    use std::f64;

    #[test]
    fn it_works() {
        assert_eq!(2, 2);
    }

    #[test]
    fn circle() {
        let circle = Circle {x: 0., y: 0., r: 1.,};
        assert_eqf64!(circle.area(), f64::consts::PI);
        let circle = Circle {x: 0., y: 0., r: 2.,};
        assert_eqf64!(circle.area(), f64::consts::PI * 2. * 2.);
        let circle = Circle {x: 0., y: 0., r: 2.,};
        print_area(circle);
    }
    
    #[test]
    fn is_larger() {
        let circle = Circle {x: 0., y: 0., r: 2.,};
        let square = Square {x: 0., y:0., side:4.};
        
        assert!(square.is_larger(&circle));
    }

    #[test]
    fn is_square() {
        // a square
        let rectangle = Rectangle{x: 0., y: 0., height: 10., width: 10.};
        assert!(rectangle.is_square());
        // not a square
        let rectangle = Rectangle{x: 0., y: 0., height: 11., width: 10.};
        assert!(!rectangle.is_square());
    }
}


